from django.urls import path
from . import views


urlpatterns = [
    path('blocklist/', views.BlockListAPI.as_view()),
    path('blocklist/<int:id>', views.BlockListAPI.as_view()),
    path('accesslog/', views.AccessLogAPI.as_view()),
    path('accesslog/<int:id>', views.AccessLogAPI.as_view()),
    path('redirect/', views.DefaultRedirectionAPI.as_view()),
    path('redirect/<int:id>', views.DefaultRedirectionAPI.as_view())
]
