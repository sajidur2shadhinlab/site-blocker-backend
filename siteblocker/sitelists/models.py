from django.db import models
from django.contrib.auth import get_user_model
# Create your models here.

User = get_user_model()


class BlockList(models.Model):
    title = models.CharField(max_length=100)
    site_url = models.CharField(max_length=255)
    redirect_url = models.CharField(max_length=255)
    custom_message = models.TextField()
    block_type = models.CharField(max_length=20)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']

    @property
    def author(self):
        return self.owner_set.all()

    def __str__(self):
        return self.title


class DefaultRedirection(models.Model):
    title = models.CharField(max_length=100)
    redirect_url = models.CharField(max_length=255)
    custom_message = models.TextField()
    owner = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']

    @property
    def author(self):
        return self.owner_set.all()

    def __str__(self):
        return self.title


class AccessLog(models.Model):
    title = models.CharField(max_length=100)
    site_url = models.CharField(max_length=255)
    redirect_url = models.CharField(max_length=255)
    custom_message = models.TextField()
    block_type = models.CharField(max_length=20)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']

    @property
    def author(self):
        return self.owner_set.all()

    def __str__(self):
        return self.title
