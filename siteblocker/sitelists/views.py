from django.shortcuts import render
from .models import BlockList, DefaultRedirection, AccessLog
from .serializers import BlockListGetSerializer, BlockListCreateSerializer, BlockListUpdateSerializer, AccessLogCreateSerializer, AccessLogGetSerializer, DefaultRedirectionGetSerializer, DefaultRedirectionCreateSerializer, DefaultRedirectionUpdateSerializer
from rest_framework import status
from rest_framework.decorators import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication


class BlockListAPI(APIView):

    authentication_classes = [JWTAuthentication, SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            block_list = BlockList.objects.filter(owner_id=request.user.id)
            serializer = BlockListGetSerializer(block_list, many=True)
            return Response(serializer.data)
        except BlockList.DoesNotExist:
            raise Http404

    def get_object(self, request, id, format=None):
        try:
            return BlockList.objects.get(id=id)
        except BlockList.DoesNotExist:
            return Http404

    def post(self, request, format=None):
        serializer = BlockListCreateSerializer(data=request.data)
        owner = request.user
        if serializer.is_valid():
            serializer.save(owner=owner)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def patch(self, request, id, format=None):
        block_site = BlockList.objects.get(id=id)

        if block_site.owner_id != request.user.id:
            raise Http404

        serializer = BlockListUpdateSerializer(block_site, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

        get_serializer = BlockListGetSerializer(block_site)
        return Response(get_serializer.data)

    def delete(self, request, id, format=None):
        block_site = BlockList.objects.get(id=id)
        if block_site.owner_id != request.user.id:
            raise Http404
        block_site.delete()
        return Response({'msg': 'Deleted'})


class AccessLogAPI(APIView):

    authentication_classes = [JWTAuthentication, SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            access_log = AccessLog.objects.filter(owner_id=request.user.id)
            serializer = AccessLogGetSerializer(access_log, many=True)
            return Response(serializer.data)
        except AccessLog.DoesNotExist:
            raise Http404

    def get_object(self, request, id, format=None):
        try:
            return AccessLog.objects.get(id=id)
        except AccessLog.DoesNotExist:
            return Http404

    def post(self, request, format=None):
        serializer = AccessLogCreateSerializer(data=request.data)
        owner = request.user
        if serializer.is_valid():
            serializer.save(owner=owner)
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class DefaultRedirectionAPI(APIView):

    authentication_classes = [JWTAuthentication, SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        try:
            redirection = DefaultRedirection.objects.filter(owner_id=request.user.id)
            serializer = DefaultRedirectionGetSerializer(redirection, many=True)
            return Response(serializer.data)
        except DefaultRedirection.DoesNotExist:
            raise Http404

    def get_object(self, request, id, format=None):
        try:
            return DefaultRedirection.objects.get(id=id)
        except DefaultRedirection.DoesNotExist:
            return Http404

    def post(self, request, format=None):
        serializer = DefaultRedirectionCreateSerializer(data=request.data)
        owner = request.user
        if serializer.is_valid():
            serializer.save(owner=owner)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def patch(self, request, id, format=None):
        redirection = DefaultRedirection.objects.get(id=id)

        if redirection.owner_id != request.user.id:
            raise Http404

        serializer = DefaultRedirectionUpdateSerializer(redirection, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

        get_serializer = DefaultRedirectionGetSerializer(redirection)
        return Response(get_serializer.data)

    def delete(self, request, id, format=None):
        redirection = DefaultRedirection.objects.get(id=id)
        if redirection.owner_id != request.user.id:
            raise Http404
        redirection.delete()
        return Response({'msg': 'Deleted'})
