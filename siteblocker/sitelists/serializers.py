from rest_framework import serializers
from .models import BlockList, AccessLog, DefaultRedirection


class BlockListGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlockList
        fields = '__all__'


class BlockListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlockList
        fields = ('id', 'title', 'site_url', 'redirect_url', 'custom_message', 'block_type', 'owner', 'created_at', 'updated_at')


class BlockListUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlockList
        fields = ('title', 'site_url', 'redirect_url', 'custom_message', 'block_type')


class AccessLogGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccessLog
        fields = '__all__'


class AccessLogCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccessLog
        fields = ('id', 'title', 'site_url', 'redirect_url', 'custom_message', 'block_type', 'owner', 'created_at', 'updated_at')


class DefaultRedirectionGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = DefaultRedirection
        fields = '__all__'


class DefaultRedirectionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DefaultRedirection
        fields = ('id', 'title', 'redirect_url', 'custom_message', 'owner', 'created_at', 'updated_at')


class DefaultRedirectionUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DefaultRedirection
        fields = ('title',  'redirect_url', 'custom_message')
