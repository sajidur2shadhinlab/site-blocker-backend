from django.db import models
from django.contrib.auth.models import AbstractUser
from .manager import UserManager


class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    mobile = models.CharField(max_length=16)
    is_verified = models.BooleanField(default=False)
    role = models.CharField(max_length=20)
    gender = models.CharField(max_length=10, null=True)
    country = models.CharField(max_length=50, null=True)
    dob = models.DateField(null=True)
    date_joined = models.DateTimeField(auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["first_name", "last_name", "mobile", "gender", "country", "dob"]
