from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, first_name, last_name, mobile, gender, country, dob, role='USER', password=None, **extra_fields):

        fields = [email, first_name, last_name, mobile, role]
        for field in fields:
            if not field:
                raise ValueError(str(field).replace('_', ' ').title().replace(' ', '') + ' is required')

        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, mobile=mobile, gender=gender, country=country, dob=dob, role=role, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, mobile, gender, country, dob, role='ADMIN', password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(('Super user must have is_staff true'))

        return self.create_user(email=email, first_name=first_name, last_name=last_name, mobile=mobile, gender=gender, country=country, dob=dob, role=role, password=password, **extra_fields)
