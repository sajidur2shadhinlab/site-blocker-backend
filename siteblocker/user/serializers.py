from rest_framework import serializers
from django.contrib.auth import get_user_model


# User Serializer
User = get_user_model()  # cause of custom user model


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'mobile', 'email', 'role', 'gender', 'country', 'dob')


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'mobile', 'email', 'gender', 'country', 'dob', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            mobile=validated_data['mobile'],
            gender=validated_data['gender'],
            country=validated_data['country'],
            dob=validated_data['dob'],
            password=validated_data['password'])
        return user
