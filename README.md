# database

```
siteblocker
```

# env
```
SECRET_KEY=django-insecure-rh&uocl9lfgj0#d57d@2lsprsgk^mmxgokyveoo9s=p&z)v=+v
DEBUG=True
DATABASE_NAME=siteblocker
DATABASE_USER=root
DATABASE_PASSWORD=''
DATABASE_HOST=localhost
DATABASE_PORT=3306
```

# install

```
pip3 install -r requirements.txt
```

# run

```
cd site-blocker-backend
python3 -m venv env
source env/bin/activate
cd siteblocker
python3 manage.py runserver
```